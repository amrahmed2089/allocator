# Allocater - v1.0

A helper tool to identify, create & inject custom predefined attribute keys inside your angular project.

## Supported Frameworks
 - Angular v11+

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install Allocate.

```bash
npm install allocate
```

## Usage

To use it inside your project directly

```bash
  npm allocate .
```

To use it on a specific file

```bash
  npm allocate "Absolute path to an html file"
```

To use it on a project

```bash
  npm allocate "Absolute path to a project directory"
```

## Contributing
Contributions are always welcome!


## License
[ISC](https://opensource.org/licenses/ISC)

## Author
[AmrAly](https://www.linkedin.com/in/amrahmedaly/)