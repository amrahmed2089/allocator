#!/usr/bin/env node
"use-strict";
const HTMLParser = require("node-html-parser");
const { COPYFILE_EXCL } = require("constants");
const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const log = console.log;
const cliProgress = require("cli-progress");
//--------------
// Loop over pre-defined tag names [INPUT | SELECT | CHECKBOX | RADIO BUTTON | BUTTON | DATE PICKER | TIME PICKER]
// let GLOBAL_PROJECT_PATH = "";
let GLOBAL_NATIVE_ATTR_NAME = "test_id";
let GLOBAL_CUSTOM_ATTR_NAME = "[test_id]"; // Assuming that the custom component already have an input with name (test_id)
let GLOBAL_DIRECT_ATTR_NAME = "[attr.test_id]"; // Assuming that the custom component already have an input with name (test_id)
let GLOBAL_TARGET_CUSTOM_ELEMENTS = ["app-list-form", "app-auto-complete", "app-input-text", "app-save-cancel"];
let GLOBAL_TARGET_NATIVE_ELEMENTS = ["input", "select", "button", "textarea", "a", "li", "ul", "i", "table"];
let APPLIED_CSS_SELECTORS = ["div[class='row p']", "[href]"];
let GLOBAL_DIR_TO_SKIP = ["node_modules", "e2e", "assets", "environments", "shared"];
let GLOBAL_FILES_TO_SKIP = [];
let GLOBAL_BACKUP_EXT_ORG = ".bak";
const GLOBAL_BACKUP_EXT = `-${new Date().toISOString().split(".")[0].replace(":", "-").replace(":", "-")}.${GLOBAL_BACKUP_EXT_ORG}`;
let GLOBAL_POSTFIX_REFERENCE = ["formControlName", "name", "id", "(click)"];
//const GLOBAL_SUFFIX_REFERENCE = ["Form"];
const GLOBAL_RUN_ID = Date.now();
const GLOBAL_RUN_FILENAME = "ChangeReport_Run_";
const progressBar = new cliProgress.SingleBar({
	format: "--> Progress |" + chalk.blue("{bar}") + "| {percentage}% || {value}/{total} Pages",
	barCompleteChar: "\u2588",
	barIncompleteChar: "\u2591",
	hideCursor: true
});

function appendToRunReport(elementSelectorList, fileName, filePath) {
	if (elementSelectorList.length > 0) {
		fs.appendFile(`./${GLOBAL_RUN_FILENAME}${GLOBAL_RUN_ID}`, `############################${filePath}/${fileName}\r\n`, (err) => {
			if (err) throw err;
		});
		elementSelectorList.forEach((item) => {
			fs.appendFile(`./${GLOBAL_RUN_FILENAME}${GLOBAL_RUN_ID}`, `${item.el.rawTagName} -> ${item.prf}-${item.psf}\r\n`, (err) => {
				if (err) throw err;
			});
		});
	}
}

function flatten(lists) {
	return lists.reduce((a, b) => a.concat(b), []);
}

function getDirectories(srcpath) {
	return fs.readdirSync(srcpath)
		.filter((file) => !GLOBAL_FILES_TO_SKIP.includes(file))
		.filter((file) => !GLOBAL_DIR_TO_SKIP.includes(file))
		.map((file) => path.join(srcpath, file))
		.filter((path) => fs.statSync(path).isDirectory());
}

function getDirectoriesRecursive(srcpath) {
	return [srcpath, ...flatten(getDirectories(srcpath).map(getDirectoriesRecursive))];
}

function getAllHtmlFiles(projectPath) {
	return getDirectoriesRecursive(projectPath).map((dir) => fs.readdirSync(dir)
		.filter((file) => !GLOBAL_FILES_TO_SKIP.includes(file))
		.filter((file) => path.extname(file) == ".html")
		.map((file) => [dir, file]));
}

function isItArealHTML(filePath, fileName) { // Check if it a one line HTML
	return fs.readFileSync(`${filePath}/${fileName}`).toString().split("\n").length > 1;
}

function filterInValidFiles(fileArray) {
	return fileArray.filter((fileItem) => isItArealHTML(fileItem[0], fileItem[1]));
}

function backUpFile(file) {
	try {
		fs.copyFileSync(file, `${file}${GLOBAL_BACKUP_EXT_ORG}`, COPYFILE_EXCL);
	} catch (error) {
		fs.copyFileSync(file, `${file}${GLOBAL_BACKUP_EXT}`, COPYFILE_EXCL);
	}
}

function captureFilePrefix(fileName) {
	return fileName.replace(".component.html", "");
}

// eslint-disable-next-line no-unused-vars
function captureElementSuffix(htmlElement) {
	// var postfix = ""
	// GLOBAL_POSTFIX_REFERENCE.some((item) => {
	//   let elementAttr = htmlElement.getAttribute(item)
	//   if (elementAttr != undefined) {
	//     postfix = elementAttr
	//     return true;
	//   }
	// })
	// return postfix
}

function captureElementPostFix(htmlElement) {
	let postfix = "";

	GLOBAL_POSTFIX_REFERENCE.some((item) => {
		const elementAttr = htmlElement.getAttribute(item);

		if (elementAttr != undefined) {
			postfix = elementAttr;
			return true;
		}
	});
	return postfix.replace(/\W/gm, "");
}

function loadDefinedElements(parserObject) {
	const htmlElementsArray = [];

	GLOBAL_TARGET_CUSTOM_ELEMENTS.concat(GLOBAL_TARGET_NATIVE_ELEMENTS).concat(APPLIED_CSS_SELECTORS).forEach((elementTag) => {
		parserObject.querySelectorAll(elementTag).forEach((element) => htmlElementsArray.push(element));
	});
	return htmlElementsArray;
}

function injectAttribute(element, attr_key, attr_value) {
	if (arguments.length < 2) {
		throw new Error("Failed to execute 'setAttribute' on 'Element'");
	}
	element.rawAttrs = `${element.rawAttrs} ${attr_key}="${attr_value}"`;
}

// function loadQualifiedElementsWithAttributes(parserObject){
// 	const htmlElementsArray = [];

// 	GLOBAL_TARGET_CUSTOM_ELEMENTS.concat(GLOBAL_TARGET_NATIVE_ELEMENTS).forEach((elementTag) => {
// 		parserObject.querySelectorAll(elementTag).forEach((element) => htmlElementsArray.push(element));
// 	});
// 	return htmlElementsArray;
// }

function injectSelector(elementsArray) {
	elementsArray.forEach((element) => {
		if (GLOBAL_TARGET_NATIVE_ELEMENTS.includes(element.el.rawTagName)) injectAttribute(element.el, GLOBAL_NATIVE_ATTR_NAME, `${element.prf}-${element.psf}`);
		else if (GLOBAL_TARGET_CUSTOM_ELEMENTS.includes(element.el.rawTagName)) injectAttribute(element.el, GLOBAL_CUSTOM_ATTR_NAME, `'${element.prf}-${element.psf}'`);
		else  injectAttribute(element.el, GLOBAL_DIRECT_ATTR_NAME, `'${element.prf}-${element.psf}'`);
	});
}

function generateCustom(HTMLelement) {
	return `${HTMLelement.rawTagName}_${Math.round(Math.random() * 10000000)}`;
}

function validateGeneratedSelector(selectorObject) {
	return {
		el: selectorObject.el,
		prf: selectorObject.prf,
		suf: selectorObject.suf,
		psf: selectorObject.psf == "" ? generateCustom(selectorObject.el) : selectorObject.psf,
	};
}

function createElementsSelector(htmlElementArraySource, fileName) {
	const filePrefix = captureFilePrefix(fileName);

	return htmlElementArraySource.map((element) => {
		if (!element.hasAttribute(GLOBAL_NATIVE_ATTR_NAME)) {
			return validateGeneratedSelector({
				el: element,
				prf: filePrefix.replace(".html", ""),
				suf: captureElementSuffix(element),
				psf: captureElementPostFix(element),
			});
		}
	}).filter((item) => item != undefined);
}

function processASingleFileChain(filePath, fileName) {
	backUpFile(`${filePath}/${fileName}`);
	const fileContent = fs.readFileSync(`${filePath}/${fileName}`);
	const root = HTMLParser.parse(fileContent, {
		lowerCaseTagName: false,
		comment: true
	});
	
	const elementsSelectors = createElementsSelector(loadDefinedElements(root), fileName);

	injectSelector(elementsSelectors);
	fs.writeFileSync(`${filePath}/${fileName}`, root.toString());
	appendToRunReport(elementsSelectors, fileName, filePath);
}

function scanAndProcess(projectPath) {
	const FilesArray = flatten(getAllHtmlFiles(projectPath).filter((e) => e.length));
	const filterInValid = filterInValidFiles(FilesArray);
	progressBar.start(filterInValid.length, 0);
	
	filterInValid.forEach((item) => {
		processASingleFileChain(item[0], item[1]);
		progressBar.increment(1);
	});

	progressBar.stop();
	log(chalk.white(`--> Finished processing ${chalk.green(filterInValid.length)} files`));
}

function loadConfig (configs)  {
	GLOBAL_NATIVE_ATTR_NAME = configs.native_attr_name;
	GLOBAL_CUSTOM_ATTR_NAME = configs.custom_attr_name;
	GLOBAL_TARGET_CUSTOM_ELEMENTS = configs.target_custom_elm;
	GLOBAL_TARGET_NATIVE_ELEMENTS = configs.target_native_elm;
	GLOBAL_DIR_TO_SKIP = configs.dir_to_skip;
	GLOBAL_FILES_TO_SKIP = configs.files_to_skip;
	GLOBAL_POSTFIX_REFERENCE = configs.postfix_reference;
	GLOBAL_BACKUP_EXT_ORG = configs.backup_ext;
	APPLIED_CSS_SELECTORS = configs.target_css_selectors;
	GLOBAL_DIRECT_ATTR_NAME = configs.reflect_attr_name;
}

// exports.scanProject = (projectPath) => {
// 	scanAndProcess(projectPath);
// };

// exports.processFile = (filePath) => {
// 	processASingleFileChain(filePath.replace(path.basename(filePath) + "/", ""), filePath);
// };

let globalConfig = {
	native_attr_name: "test_id",
	custom_attr_name: "[test_id]",
	reflect_attr_name: "[attr.test_id]",
	target_custom_elm: ["app-list-form", "app-auto-complete", "app-input-text", "app-save-cancel"],
	target_native_elm: ["input", "select", "button", "textarea"],
	dir_to_skip: ["node_modules", "e2e", "assets", "environments", "shared"],
	files_to_skip: [],
	backup_ext: ".allocate_bak",
	postfix_reference: ["formControlName", "name", "id", "(click)"],
	target_css_selectors: ["div[class='row p']", "[href]"]
};

function getRoot() {
	// eslint-disable-next-line no-undef
	return process.cwd();
}

function validateFileMetaKeysValues() {
	const metaKeys = ["native_attr_name", "custom_attr_name", "target_custom_elm", "target_native_elm", "dir_to_skip", "backup_ext", "postfix_reference"];
	return metaKeys.every((item) => Object.keys(globalConfig).includes(item) && globalConfig[item] != "" && globalConfig[item] != null);
}

function validateFileExistance() {
	if (!fs.existsSync(path.resolve(`${getRoot()}/config.json`))) {
		fs.writeFileSync(path.resolve(`${getRoot()}/config.json`), JSON.stringify(globalConfig));
	}
	globalConfig = require(path.resolve(`${getRoot()}/config.json`));
	return true;
}

function validateConfigFile() {
	const fileValidations = [validateFileExistance, validateFileMetaKeysValues];
	return fileValidations.every((item) => item() == true);
}

log(chalk.white("--> Starting alocate library."));
log(chalk.white(`--> Check your config file status: ${(validateConfigFile()? chalk.green("Valid") : chalk.red("Invalid"))}`));
loadConfig(globalConfig);

//eslint-disable-next-line no-undef
const processArgs = process.argv;
if (processArgs.length == 3){
	if (path.basename(processArgs[2]).endsWith(".html")) {
		processASingleFileChain(processArgs[2].replace(path.basename(processArgs[2]), ""), path.basename(processArgs[2]));
	} else if (fs.statSync(processArgs[2]).isDirectory()) {
		scanAndProcess(processArgs[2]);
	}
}else{
	log(chalk.red("--> Sorry, you have to specify a project/file absoulte path to be processed."));
}


// Get the target folder as input & Attribute key name [Test_id]
// Get list of HTML files in all sub folders of the project - Done
// Mark which is a real Html and exclude the rest from the list - Done
// Create backup file in the same processed file location with additional ext (.bak) - Done
// PREFIX -- Identify component name by last (Folder name | File name) = [Prefix] - Done
// = Locator = [TestID]= Start - Middle - End = User_ALlUsers_EmployeeID - Done
// Identify Suffix by the most nearest parent with tag [Form | Div] By [ID | Form Group Name | Form Array Name] =  Sufix - Next release
// Identify Postfix by the current element [ID | Form control name | Name] = Postfix - Done

// Store each generated (Attribute) in a list with [Element tag, Name, File & Line] - Done
// Generate a report with generated (Attributes) data - Done
// Add a rollbak feature
//------------------

// If it an HTML file for a custom component -> Skip -> How to identify if it is a custom component html ?
// By Ignoring the folder before running? -> Add to ignored folders
// By matching the file name with custom tags array?
// By checking the selector of the file? if it matches any of the custom tags array?

// Then check if this component has an input with name (test_id)
// Search the (ts) file line by line?

// If not then add it in (html) and (ts) file
// Where to insert as string?
// By identifying the last line with (@input()) string?

// Then use that inserted input in any tag with selector name of the custom component
// It should be stored and identifyed by our script for future usage and to avoid any duplicates

// Solution for lists